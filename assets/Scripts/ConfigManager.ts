import Singleton from "./Common/Singleton";
import Config from "./Common/Config";
import { ConfigType } from "./Common/GameDefine";
import EquipConfig from "./Config/EquipAttrConfig";
import ItemConfig from "./Config/ItemConfig";
import MonsterConfig from "./Config/MonsterConfig";
import MapConfig from "./Config/MapConfig";
import AttrColorConfig from "./Config/AttrColorConfig";

class ConfigManager extends Singleton<ConfigManager>{
    //配置实例
    private m_configs: Map<ConfigType, Config> = new Map<ConfigType, Config>();

    public init(): void {

        //装备配置
        let equipCfg: EquipConfig = new EquipConfig();
        this.m_configs.set(equipCfg.m_type, equipCfg);

        //物品配置
        let itemCfg: ItemConfig = new ItemConfig();
        this.m_configs.set(itemCfg.m_type, itemCfg);

        //怪物配置
        let monsterCfg: MonsterConfig = new MonsterConfig();
        this.m_configs.set(monsterCfg.m_type, monsterCfg);

        //map配置
        let mapCfg: MapConfig = new MapConfig();
        this.m_configs.set(mapCfg.m_type, mapCfg);

        //属性配置
        let attrColorCfg: AttrColorConfig = new AttrColorConfig();
        this.m_configs.set(attrColorCfg.m_type, attrColorCfg);

        this.m_configs.forEach((value, key) => {
            value.init();
        })
    }

    public getConfigByType(type: ConfigType): Config {
        return this.m_configs.get(type);
    }
}

let gConfigManager:ConfigManager = ConfigManager.instance.bind(ConfigManager, ConfigManager)();
export default gConfigManager;