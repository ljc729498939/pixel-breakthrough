import Singleton from "./Common/Singleton";
import gConfigManager from "./ConfigManager";
import Player from "./Player/Player";
import MapManager from "./MapManager";
import { ConfigType, ArchiveType, EqupAttrType } from "./Common/GameDefine";
import Drugs from "./Item/Drugs";
import Equip from "./Equip/Equip";
import gArchiveManager from "./ArchiveManager";
import EquipAttr from "./Equip/EquipAttr";
import ItemConfig from "./Config/ItemConfig";
import gItemFactory from "./ItemFactory";



export class GameManager extends Singleton<GameManager>{
    m_player: Player = new Player();//玩家实例
    m_mapManager: MapManager = new MapManager();//地图实例
    
    public init():void{
        gConfigManager.init();
        gItemFactory.init();

        //存档初始化
        let archiveMg = gArchiveManager.instance();
        archiveMg.init();

        this.m_mapManager.init();
        this.m_player.init();

        let cfg = gConfigManager.getConfigByType(ConfigType.CT_MAP);
        //let itemCfg = cfg.getCfgById(1);
        //console.log(itemCfg);

        archiveMg.regArchiveObj(ArchiveType.AT_PLAYER, this.m_player);
        archiveMg.regArchiveObj(ArchiveType.AT_GAME_MAP, this.m_mapManager);

        // archiveMg.save();
        // this.m_player.reduceInventory(item);
        // //console.log(this.m_player);

        // archiveMg.read();

        // console.log(this.m_player.m_bag.m_items[0]);
    }

    public test():void{

        this.m_player.m_roleId = 1;
        this.m_player.m_baseAttr.m_atk = 5;
        this.m_player.m_baseAttr.m_hp = 100000;

        let itemCfg: ItemConfig = <ItemConfig>gConfigManager.getConfigByType(ConfigType.CT_ITEM);
        let cfg = itemCfg.getCfgById(1);

        let item = new Equip();
        item.m_id = cfg.id;
        item.m_type = cfg.type;
        item.m_equipType = cfg.equip_type;
        item.m_forging = 4;

        item.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 1));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 1));

        item.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 2));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 2));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_EVA, 10, 2));

        item.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 3));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 3));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_EVA, 10, 3));

        item.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 4));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 4));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_EVA, 10, 4));

        item.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 5));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 5));
        item.addAttr(new EquipAttr(EqupAttrType.EAT_EVA, 10, 5));

        this.m_player.addItem(item);


        cfg = itemCfg.getCfgById(2);

        let item2 = new Equip();
        item2.m_id = cfg.id;
        item2.m_type = cfg.type;
        item2.m_equipType = cfg.equip_type;
        item2.m_forging = 6;
        item2.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 1));
        item2.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 1));
        item2.addAttr(new EquipAttr(EqupAttrType.EAT_DEF, 100, 3));
        item2.addAttr(new EquipAttr(EqupAttrType.EAT_HP, 1000, 3));
        item2.addAttr(new EquipAttr(EqupAttrType.EAT_EVA, 10, 3));


        this.m_player.addItem(item2);

        let item3 = new Drugs();
        item3.m_id = 5;
        item3.m_count = 10;
        this.m_player.addItem(item3);
    }
}
let gGameManager = GameManager.instance.bind(GameManager, GameManager)();
export default gGameManager;
