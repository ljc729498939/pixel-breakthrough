import Item from "./Item";
import IItemUseTarget from "../Common/IItemUseTarget";

export default class Drugs extends Item{

    public use(target: IItemUseTarget): void {
        this.m_owner.reduceInventory(this);
    }

}