import IItemUseTarget from "../Common/IItemUseTarget";
import { ItemType } from "../Common/GameDefine";
import Player from "../Player/Player";
import ItemView from "../View/ItemView";

export default abstract class Item {
    m_owner: Player = null;//所有者
    m_type: ItemType = ItemType.IT_NONE; //物品类型
    m_count: number = 1; //物品数量
    m_isStacking: boolean = true; //是否可堆叠
    m_id: number = 0;//物品id
    m_itemView: ItemView = null;//视图对象

    public constructor(){
        Object.defineProperty(this, "m_owner", { enumerable: false })
        Object.defineProperty(this, "m_itemView", { enumerable: false })
    }

    public noticViewChange(remove: boolean = false):void{
        if(this.m_itemView){
            this.m_itemView.onChange(remove);
        }
    }

    public abstract use(target: IItemUseTarget): void;
}
Object.defineProperty(Item, "m_owner", { enumerable: false })