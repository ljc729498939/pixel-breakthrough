import BaseAttr from "./BaseAttr";

export default interface IAttackTarget{
    m_baseAttr: BaseAttr;
    //攻击
    attack(target: IAttackTarget):void;
    //被攻击
    onAttack(target: IAttackTarget):void;
}