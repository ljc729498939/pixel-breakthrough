
export default class Util {

    /**
     * 产生(min, max]区间的随机整数
     * @param min 最小值(含)
     * @param max 最大值(不含)
     */
    public static randomInt(min: number, max: number):number{
        return Math.floor(Math.random() * (max - min)) + min;
    }
}