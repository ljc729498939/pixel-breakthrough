import {ConfigType} from "./GameDefine";

export default interface Config {
    m_type: ConfigType;

    init(): void;
}