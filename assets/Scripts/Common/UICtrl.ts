
export default class UICtrl extends cc.Component{

    //保存当前节点的所有节点映射关系
    protected view: Map<string, cc.Node> = new Map<string, cc.Node>();

    private LoadAllNode(root: cc.Node, path: string):void{
        for(let i = 0; i < root.childrenCount; ++i){
            let childrenPath = `${path}${root.children[i].name}`;
            this.view[childrenPath] = root.children[i];
            this.LoadAllNode(root.children[i], childrenPath + "/");
        }
    }

    protected onLoad():void{
        this.LoadAllNode(this.node, "");
    }
}