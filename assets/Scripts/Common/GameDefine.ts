
//配置类型
export enum ConfigType{
    CT_DEFAULT = 0, //默认值
    CT_MONSTER = 1, //怪物
    CT_ITEM = 2, //物品
    CT_EQUIP_ATTR = 3, //装备属性
    CT_MAP = 4,//地图配置
    CT_ATTR_COLOR = 5,//属性配置
}

//物品类型
export enum ItemType{
    IT_NONE = 0, //无效
    IT_DRUGS = 1,//药品
    IT_EQUIP = 2,//装备
}

//装备类型
export enum EquipType{
    ET_NONE = 0,
    ET_WEAPON = 1, //武器
    ET_ARMOR = 2, //护甲
    ET_SHOES = 3, //鞋子
    ET_HANDGUARD = 4, //护手
    ET_BRACELET = 5, //手镯
    ET_NECKLACE = 6, //项链
    ET_HELMET = 7, //头盔
}

//存档类型
export enum ArchiveType{
    AT_PLAYER = "PB_PLAYER",
    AT_GAME_MAP = "PB_GAME_MAP",
}

//颜色配置
export enum EquipColor{
    ET_INVALID = 0,
    ET_WHILE = 1,
    ET_BLUE = 2,
    ET_GREEN = 3,
    ET_ORANGE = 4,
    ET_RED = 5
}

//装备属性类型
export enum EqupAttrType{
    EAT_NONE = 0,
    EAT_HP  = 1, //生命
    EAT_SP  = 2, //魔法
    EAT_ATK = 3, //攻击
    EAT_DEF = 4, //防御
    EAT_ACC = 5, //命中
    EAT_EVA = 6, //闪避
    EAT_AHP = 7, //吸血
    EAT_ASP = 8, //吸魔
    EAT_CRI = 9, //暴击
    EAT_VER = 10, //眩晕
    EAT_POI = 11, //中毒
    EAT_STR = 12, //力量
    EAT_CON = 13, //体质
    EAT_DEX = 14, //敏捷
    EAT_INT = 15, //智力
    EAT_PSR = 16, //毒抗
    EAT_VGR = 17, //晕抗
}

//装备状态
export enum EquipStatus {
    ES_NONE = 0,
    ES_BAG = 1,//在背包
    ES_WEAR = 2,//装备在身上
    ES_MAKE = 3,//正在打造
}