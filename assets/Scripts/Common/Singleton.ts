
export default class Singleton<T>
{
    private static _instace: any = null;
    protected constructor(){}
    public static instance<T>(c: { new(): T }): T{
        if(!this._instace){
            this._instace = new c();
        }
        return this._instace;
    }
}