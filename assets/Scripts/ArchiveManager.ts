import Singleton from "./Common/Singleton";
import { ArchiveType } from "./Common/GameDefine";
import ArchiveTarget from "./Common/ArchiveTarget";


export class ArchiveManager extends Singleton<ArchiveManager>{
    m_objs: Map<string, ArchiveTarget> = new Map<string, ArchiveTarget>();
    m_str: string = "";

    public init(): void {

    }

    //注册存档对象
    public regArchiveObj(aType: ArchiveType, obj: Object): void {
        this.m_objs.set(aType, obj);
    }

    //存档
    public save(): void {
        let target = {};
        this.m_objs.forEach((val, key) => {
            target[key] = val;
        });
        this.m_str = JSON.stringify(target);
    }

    //读档
    public read(): void {
        let target = JSON.parse(this.m_str);
        this.m_objs.forEach((val, key) => {
            Object.keys(target[key]).forEach(idx=>{val[idx]=target[key][idx];})
        });
    }
}

namespace gArchiveManager {
    export let instance = ArchiveManager.instance.bind(ArchiveManager, ArchiveManager)
}
export default gArchiveManager;