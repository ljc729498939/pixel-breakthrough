import IAttackTarget from "../Common/IAttackTarget";
import BaseAttr from "../Common/BaseAttr";
import MonsterManager from "../Map/MonsterManager";
import MonsterView from "../View/MonsterView";
import Player from "../Player/Player";
import gItemFactory from "../ItemFactory";

export default class Monster implements IAttackTarget {
    m_id: number = 0;//怪物id
    m_name: string = "";//怪物名称
    m_monsterMg: MonsterManager = null;
    m_baseAttr: BaseAttr = new BaseAttr();
    m_monsterView: MonsterView = null;

    public constructor(){
        Object.defineProperty(this, "m_monsterMg", { enumerable: false })
    }

    public attack(target: IAttackTarget): void {
        target.onAttack(this);
    }

    public onAttack(target: IAttackTarget): void {
        //根据对方输出计算伤害
        let hp = target.m_baseAttr.m_atk - this.m_baseAttr.m_def;
        if (hp < 0) {
            hp = 0;
        }

        this.m_baseAttr.m_hp -= hp;

        this.m_monsterView.onAttack(hp);

        if (this.m_baseAttr.m_hp <= 0) {
            this.m_monsterMg.removeMonster(this);
            (<Player>target).addItem(gItemFactory.createItemByScore(0));
        }
    }
}