import Singleton from "./Common/Singleton";
import TipView from "./View/TipView";

const { ccclass, property } = cc._decorator;

@ccclass
export class TipManager extends Singleton<TipManager>{

    tip_prefab: cc.Prefab = null;

    node_pool: cc.NodePool = new cc.NodePool("tip_pool");

    parent_node: cc.Node = null;

    public init(parent: cc.Node):void{
        this.parent_node = parent;
        cc.resources.load("prefab/tips", cc.Prefab, (err, asset)=>{
            if(!err){
                this.tip_prefab = <cc.Prefab>asset;
                this.initNodePool();
            }
        });
    }

    public put(node: cc.Node):void{
        this.node_pool.put(node);
        node.opacity = 255;
    }

    public showTip(text: string, color: cc.Color, target: cc.Node):void{
        if(this.node_pool.size() <= 0){
            let node = cc.instantiate(this.tip_prefab);
            this.node_pool.put(node);
        }

        let node = this.node_pool.get();
        node.parent = target.parent;
        node.getComponent(TipView).show(text, color, target.getPosition());
    }

    private initNodePool():void{
        for(let i = 0; i < 20; ++i){
            let node = cc.instantiate(this.tip_prefab);
            this.node_pool.put(node);
        }
    }
}

namespace gTipManager{
    export let instance = TipManager.instance.bind(TipManager, TipManager)
}
export default gTipManager;
