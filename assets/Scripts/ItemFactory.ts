import Singleton from "./Common/Singleton";
import Item from "./Item/Item";
import Equip from "./Equip/Equip";
import { EquipType, ConfigType } from "./Common/GameDefine";
import Util from "./Common/Util";
import EquipAttr from "./Equip/EquipAttr";
import gConfigManager from "./ConfigManager";
import EquipAttrConfig from "./Config/EquipAttrConfig";

export class ItemFacotry extends Singleton<ItemFacotry>{

    private equipAttrCfg: EquipAttrConfig = null;

    public init(): void {
        this.equipAttrCfg = <EquipAttrConfig>gConfigManager.getConfigByType(ConfigType.CT_EQUIP_ATTR);
    }

    public createItemByScore(score: number): Item {
        //高级属性爆率
        let pro2 = 60;
        //附魔属性爆率
        let pro3 = 40;
        //转生属性爆率
        let pro4 = 20;
        //觉醒属性爆率
        let pro5 = 5;

        //暂时每次都爆一件装备
        let equip: Equip = new Equip();
        equip.m_id = 9;//固定id
        equip.m_equipType = EquipType.ET_WEAPON;//固定是武器
        equip.m_forging = Util.randomInt(0, 6) + 1;//随机1-6星
        equip.addAttr(new EquipAttr(3, 100, 1));//普通属性固定100

        this.randomAddEqupAttr(equip, 2, pro2);
        this.randomAddEqupAttr(equip, 3, pro3);
        this.randomAddEqupAttr(equip, 4, pro4);
        this.randomAddEqupAttr(equip, 5, pro5);

        return equip;
    }

    private randomAddEqupAttr(equip: Equip, color: number, pro: number): void {
        if (Util.randomInt(0, 101) < pro) {
            let count = Util.randomInt(1, 4);
            for (let i = 0; i < count; ++i) {
                let idx = Util.randomInt(0, this.equipAttrCfg.size());
                let cfg = this.equipAttrCfg.getCfgByIdx(idx);
                let val = Util.randomInt(cfg.min_value, cfg.max_value + 1);
                equip.addAttr(new EquipAttr(cfg.id, val, color));
            }
        }
    }
}

var gItemFactory: ItemFacotry = ItemFacotry.instance.bind(ItemFacotry, ItemFacotry)();

export default gItemFactory;
