import Monster from "../Monster/Monster";
import MapView from "../View/MapView";

export default class MonsterManager {
    m_monsters: Array<Monster> = [];//怪物列表
    m_mapView: MapView = null;

    //添加怪物
    public addMonster(m: Monster): void {
        if(m){
            m.m_monsterMg = this;
            this.m_monsters.push(m);
        }
    }
    //移除怪物
    public removeMonster(m: Monster): void {
        let idx = this.m_monsters.indexOf(m);
        if(idx < 0){
            return;
        }

        this.m_monsters.splice(idx, 1);

        if(this.m_mapView){
            this.m_mapView.onRemoveMonster(m);
        }
    }
}