import MonsterManager from "./MonsterManager";

export default class GameMap{
    m_mapId: number = 0;//地图id
    m_mapName: string = "";//地图名称
    m_minLevel: number = 0;//最小进入等级
    m_maxLevel: number = 0;//最大进入等级
    m_monsterMg: MonsterManager = new MonsterManager();
}