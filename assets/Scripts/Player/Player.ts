import BagManager from "./BagManager";
import EquipManager from "../Equip/EquipManager";
import IItemUseTarget from "../Common/IItemUseTarget";
import Item from "../Item/Item";
import { EquipType } from "../Common/GameDefine";
import Equip from "../Equip/Equip";
import IAttackTarget from "../Common/IAttackTarget";
import BaseAttr from "../Common/BaseAttr";
import PlayerView from "../View/PlayerView";


export default class Player implements IItemUseTarget, IAttackTarget{
    m_roleId: number = 0;//角色id
    m_bag: BagManager = new BagManager();
    m_equip: EquipManager = new EquipManager();
    m_baseAttr: BaseAttr = new BaseAttr();
    m_playerView: PlayerView = null;

    public init(): void{
        this.m_bag.init(this);
        this.m_equip.init(this);
    }

    //====战斗
    //攻击
    attack(target: IAttackTarget): void {
        target.onAttack(this);
    }
    //被攻击
    onAttack(target: IAttackTarget): void {
        //根据对方输出计算伤害
        let hp = target.m_baseAttr.m_atk - this.m_baseAttr.m_def;
        if(hp < 0){
            hp = 0;
        }

        this.m_baseAttr.m_hp -= hp;

        this.m_playerView.onAttack(hp);

        if(this.m_baseAttr.m_hp <= 0){
            //TODO 死亡处理
            this.m_playerView.onDead();
        }
    }


    //====装备管理
    //通过装备类型获取装备
    public getEquipByType(type: EquipType):Equip{
        return this.m_equip.getEquipByType(type);
    }
    //穿戴装备
    public wearEquip(ep: Equip): void{
        ep.m_owner = this;
        this.m_equip.wearEquip(ep);
    }
    //脱下装备
    public takeOffEquip(epType: EquipType): void{
        this.m_equip.takeOffEquip(epType);
    }



    //====物品管理
    //增加物品
    public addItem(item: Item):void{
        this.m_bag.addItem(item);
    }
    //减少库存
    public reduceInventory(item: Item, count: number = 1):void{
        this.m_bag.reduceInventory(item, count);
    }
    //通过索引获取物品
    public getItemByIndex(idx:number):Item{
        return this.m_bag.getItemByIndex(idx);
    }
}