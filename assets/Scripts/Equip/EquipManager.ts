import { EquipType, EquipStatus } from "../Common/GameDefine";
import Equip from "./Equip";
import Player from "../Player/Player";
import BagView from "../View/BagView";
import EquipStarView from "../View/EquipStarView";

export default class EquipManager {
    m_bagView: BagView = null;//背包视图
    m_owner: Player = null; //所有者
    m_equips: Map<EquipType, Equip> = new Map<EquipType, Equip>();//装备列表

    public constructor(){
        Object.defineProperty(this, "m_owner", { enumerable: false })
    }

    public init(owner: Player): void {
        this.m_owner = owner;
    }

    //通过装备类型获取装备
    public getEquipByType(type: EquipType):Equip{
        return this.m_equips.get(type);
    }

    //穿戴装备
    public wearEquip(ep: Equip): void{
        let oldEquip: Equip = this.m_equips.get(ep.m_equipType);
        if(oldEquip){
            //老装备脱下
            this.takeOffEquip(oldEquip.m_equipType);
        }

        //穿上新装备
        this.m_equips.set(ep.m_equipType, ep);
        if(this.m_bagView)
            this.m_bagView.wearEquip(ep);

        //TODO 刷新用户属性

        //从用户背包移除
        this.m_owner.reduceInventory(ep);

        ep.m_status = EquipStatus.ES_WEAR;
    }

    //脱下装备
    public takeOffEquip(epType: EquipType): void{
        let equip: Equip = this.m_equips.get(epType);
        if(equip){
            //从装备列表删除
            this.m_equips.delete(epType);

            this.m_bagView.takeOffEquip(equip);

            //TODO 刷新用户属性

            //放回用户背包
            this.m_owner.addItem(equip);
            equip.m_status = EquipStatus.ES_BAG;
        }
    }
}