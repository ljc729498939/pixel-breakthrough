
export default class EquipAttr{
    m_type: number = 0;//装备属性类型
    m_value: number = 0;//属性值
    m_color: number = 0;//属性颜色

    constructor(type: number, value: number, color:number){
        this.m_type = type;
        this.m_value = value;
        this.m_color = color;
    }
}