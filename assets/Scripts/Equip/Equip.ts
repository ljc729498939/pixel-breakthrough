import Item from "../Item/Item";
import IItemUseTarget from "../Common/IItemUseTarget";
import EquipAttr from "./EquipAttr";
import { EquipType, ItemType, EquipStatus } from "../Common/GameDefine";

export default class Equip extends Item {

    m_equipType: EquipType = EquipType.ET_NONE;//装备类型
    m_quality: number = 0;//品质
    m_forging: number = 0;//锻造等级
    m_attrs: Array<EquipAttr> = []; //属性列表
    m_status: EquipStatus = EquipStatus.ES_NONE;//装备状态

    public constructor() {
        super();
        this.m_isStacking = false;
        this.m_type = ItemType.IT_EQUIP;
    }

    public use(target: IItemUseTarget): void {
        //直接装备到用户身上
        this.m_owner.wearEquip(this);
    }

    public tackoff():void{
        this.m_owner.takeOffEquip(this.m_equipType);
    }

    //增加属性
    public addAttr(attr: EquipAttr):void{
        this.m_attrs.push(attr);
    }

    //替换属性
    public replaceAttr(attr: EquipAttr, idx:number):void{
        
    }

}