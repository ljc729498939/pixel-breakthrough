import Config from "../Common/Config";
import { ConfigType } from "../Common/GameDefine";

export default class AttrColorConfig implements Config {
    m_type: ConfigType = ConfigType.CT_ATTR_COLOR;

    init(): void {
    }

    public getCfgById(id: number): Object {
        for (let it of cfgItem) {
            if (it.id === id) {
                return it;
            }
        }
        return null;
    }

    public getAllCfg(): any {
        return cfgItem;
    }
}

let cfgItem = 
[
    {id: 1,		 name: "普通属性",		rgb: "#FFFFFF"},
    {id: 2,		 name: "高级属性",		rgb: "#68D5ED"},
    {id: 3,		 name: "附魔属性",		rgb: "#78FF1E"},
    {id: 4,		 name: "转生属性",		rgb: "#FFB400"},
    {id: 5,		 name: "觉醒属性",		rgb: "#FF0000"}
];