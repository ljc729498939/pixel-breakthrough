import Config from "../Common/Config";
import { ConfigType } from "../Common/GameDefine";
import NavMenuView from "../View/NavMenuView";

export default class EquipAttrConfig implements Config {
    m_type: ConfigType = ConfigType.CT_EQUIP_ATTR;
    init(): void {
    }

    public getCfgById(id: number): any {
        for (let it of cfgItem) {
            if (it.id === id) {
                return it;
            }
        }
        return null;
    }

    public getCfgByIdx(idx: number):any{
        if(idx >= this.size()){
            return null;
        }
        return cfgItem[idx];
    }

    public size(): number {
        return cfgItem.length;
    }
}

let cfgItem =
    [
        { id: 1, name: "生命", min_value: 10, max_value: 100 },
        { id: 2, name: "魔法", min_value: 10, max_value: 100 },
        { id: 3, name: "攻击", min_value: 10, max_value: 100 },
        { id: 4, name: "防御", min_value: 10, max_value: 100 },
        { id: 5, name: "命中", min_value: 10, max_value: 100 },
        { id: 6, name: "闪避", min_value: 10, max_value: 100 },
        { id: 7, name: "吸血", min_value: 10, max_value: 100 },
        { id: 8, name: "吸魔", min_value: 10, max_value: 100 },
        { id: 9, name: "暴击", min_value: 10, max_value: 100 },
        { id: 10, name: "眩晕", min_value: 10, max_value: 100 },
        { id: 11, name: "中毒", min_value: 10, max_value: 100 },
        { id: 12, name: "力量", min_value: 10, max_value: 100 },
        { id: 13, name: "体质", min_value: 10, max_value: 100 },
        { id: 14, name: "敏捷", min_value: 10, max_value: 100 },
        { id: 15, name: "智力", min_value: 10, max_value: 100 },
        { id: 16, name: "毒抗", min_value: 10, max_value: 100 },
        { id: 17, name: "晕抗", min_value: 10, max_value: 100 },
    ];