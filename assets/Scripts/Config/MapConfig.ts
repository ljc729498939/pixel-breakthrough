import Config from "../Common/Config";
import { ConfigType } from "../Common/GameDefine";

export default class MapConfig implements Config {
    m_type: ConfigType = ConfigType.CT_MAP;

    init(): void {
    }

    public getCfgById(id: number): Object {
        for (let it of cfgItem) {
            if (it.id === id) {
                return it;
            }
        }
        return null;
    }

    public getAllCfg(): any {
        return cfgItem;
    }
}

let cfgItem =
    [
        { id: 1, name: "新手森林", min_level: 0, max_level: 10, monsterIds: [1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8] },
        { id: 2, name: "死亡沙漠", min_level: 11, max_level: 20, monsterIds: [1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8] },
        { id: 3, name: "幽灵海洋", min_level: 21, max_level: 30, monsterIds: [1, 2] },
        { id: 4, name: "毁灭沼泽", min_level: 31, max_level: 40, monsterIds: [1, 2] },
        { id: 5, name: "凶险泥潭", min_level: 41, max_level: 50, monsterIds: [1, 2] },
        { id: 6, name: "龙门客栈", min_level: 51, max_level: 60, monsterIds: [1, 2] },
    ];