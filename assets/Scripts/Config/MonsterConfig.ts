import Config from "../Common/Config";
import {ConfigType} from "../Common/GameDefine";

export default class MonsterConfig implements Config {
    m_type: ConfigType = ConfigType.CT_MONSTER;
    init(): void {
    }

    public getCfgById(id: number): Object {
        for (let it of cfgMonster) {
            if (it.id === id) {
                return it;
            }
        }
        return null;
    }
}

let cfgMonster =
[
    {id: 1,     type: 0,        name: "小土狼",     image: "",      level: 0,       hp: 100},
    {id: 2,     type: 0,        name: "黑将军",     image: "",      level: 10,      hp: 10},
    {id: 3,     type: 0,        name: "大头蝎",     image: "",      level: 10,      hp: 10},
    {id: 4,     type: 0,        name: "绿巫师",     image: "",      level: 10,      hp: 10},
    {id: 5,     type: 0,        name: "紫扇龙",     image: "",      level: 10,      hp: 10},
    {id: 6,     type: 0,        name: "胖刺客",     image: "",      level: 10,      hp: 10},
    {id: 7,     type: 0,        name: "蛮力士",     image: "",      level: 10,      hp: 10},
    {id: 8,     type: 0,        name: "黑幽灵",     image: "",      level: 10,      hp: 10},
]