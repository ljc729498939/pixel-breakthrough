import Config from "../Common/Config";
import { ConfigType } from "../Common/GameDefine";

export default class ItemConfig implements Config {
    m_type: ConfigType = ConfigType.CT_ITEM;

    init(): void {
    }

    public getCfgById(id: number): any {
        for (let it of cfgItem) {
            if (it.id === id) {
                return it;
            }
        }
        return null;
    }
}

let cfgItem = 
[
    {id: 1,		 type: 2,		name: "蚕丝甲",		    is_stacking: false,		equip_type: 2, desc:"上古将军保命的护甲"},
    {id: 2,		 type: 2,		name: "祭天项链",		is_stacking: false,		equip_type: 6, desc:"为献祭而生"},
    {id: 5,		 type: 1,		name: "小血瓶",		    is_stacking: true,		equip_type: 0, desc:"回复 30 HP"},
    {id: 7,		 type: 2,		name: "银靴",		    is_stacking: false,		equip_type: 3, desc:"全身泛发着银色的光芒"},
    {id: 9,		 type: 2,		name: "绝世好贱",		is_stacking: false,		equip_type: 1, desc:"步惊云专用"},
];