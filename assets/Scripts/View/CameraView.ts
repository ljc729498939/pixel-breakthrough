import PlayerView from "./PlayerView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CameraView extends cc.Component {

    @property({
        type:cc.Node,
        displayName: "主角节点"
    })
    player_node: cc.Node = null;

    update (dt) {
        var w_pos = this.player_node.convertToWorldSpaceAR(cc.v2(0, 0));
        var c_pos = this.node.parent.convertToNodeSpaceAR(w_pos);

        this.node.x = c_pos.x;
        this.node.y = c_pos.y;
    }
}
