
const {ccclass, property} = cc._decorator;

@ccclass
export default class EquipStarView extends cc.Component {

    star_node: Array<cc.Node> = [];

    onLoad () {
        this.node.children.forEach((val, key) => {
            val.children[0].active = false;
            this.star_node.push(val.children[0]);
        })
    }

    start () {

    }

    // public init():void{
    //     this.node.children.forEach((val, key) => {
    //         val.children[0].active = false;
    //         this.star_node.push(val.children[0]);
    //     })
    // }

    public setStarLevel(level: number):void{
        if(level > this.node.childrenCount){
            level = this.node.childrenCount;
        }

        this.star_node.forEach((val, key) => {
            if(key < level){
                val.active = true;
            }
            else{
                val.active = false;
            }
        })
    }
}
