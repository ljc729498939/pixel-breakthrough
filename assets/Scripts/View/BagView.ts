
const {ccclass, property} = cc._decorator;

import Item from "../Item/Item";
import ItemView from "./ItemView";
import Equip from "../Equip/Equip";
import EquipView from "./EquipView";
import MenuBtnView from "./MenuBtnView";
import gGameManager from "../GameManager";
import { ItemType, EquipStatus } from "../Common/GameDefine";

@ccclass
export default class BagView extends cc.Component {

    @property({
        type: cc.Prefab,
        displayName: "物品预制体"
    })
    item_prefab: cc.Prefab = null;

    @property({
        type: cc.Node,
        displayName: "Content节点"
    })
    content_node: cc.Node = null;

    @property({
        type: MenuBtnView,
        displayName: "关闭按钮视图"
    })
    close_node: MenuBtnView = null;

    @property({
        type: [cc.Node],
        displayName: "装备卡槽"
    })
    equip_nodes: Array<cc.Node> = [];

    onLoad(){
        this.close_node.init("X", this.onClose.bind(this));
        //let gameMg = gGameManager.instance();
        gGameManager.m_player.m_bag.m_bagView = this;
        gGameManager.m_player.m_equip.m_bagView = this;
        //gameMg.test();
        this.node.active = false;
    }

    start () {

    }

    public addItemView(item: Item){
        let node = cc.instantiate(this.item_prefab);
        let itemView: ItemView = node.getComponent(ItemView);
        itemView.init(item);
        item.m_itemView = itemView;

        this.content_node.addChild(node);

        if(item.m_type === ItemType.IT_EQUIP){
            (<Equip>item).m_status = EquipStatus.ES_BAG;
        }
    }

    public wearEquip(equip: Equip):void{
        this.equip_nodes.forEach((val, key) => {
            let equipView:EquipView = val.getComponent(EquipView);
            if(equipView.equip_type === equip.m_equipType){
                equipView.init(equip);
            }
        });
    }

    public takeOffEquip(equip: Equip):void{
        this.equip_nodes.forEach((val, key) => {
            let equipView:EquipView = val.getComponent(EquipView);
            if(equipView.equip_type === equip.m_equipType){
                equipView.clear();
            }
        });
    }

    public onClose():void{
        this.node.active = false;
    }
}
