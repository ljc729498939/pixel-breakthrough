import ButtonView from "./ButtonView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class OperatorView extends cc.Component {

    public onLoad(){
        this.node.active = false;
    }

    public init(obj: Array<{ name, callback }>): void {

        let needShow: boolean = false;

        for (let i = 0; i < this.node.childrenCount; ++i) {
            if (i < obj.length) {
                needShow = true;
                this.node.children[i].active = true;
                let btnView: ButtonView = this.node.children[i].getComponent(ButtonView);
                btnView.init(obj[i].name, obj[i].callback);
            }
            else {
                this.node.children[i].active = false;
            }
        }

        this.node.active = needShow;
    }
}
