const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonView extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;
    
    m_cb: () => {} = null;

    start() {
        console.log("ButtonView start");
    }

    public init(name: string, callback: () => {} = null): void {
        this.m_cb = callback;
        this.label.string = name;
    }

    public onClicked(): void {
        if (this.m_cb) {
            this.m_cb();
        }
    }
}
