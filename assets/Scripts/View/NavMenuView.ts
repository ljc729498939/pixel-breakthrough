import MenuBtnView from "./MenuBtnView";

const {ccclass, property} = cc._decorator;

import GameView from "./GameView";

@ccclass
export default class NavMenuView extends cc.Component {

    @property({
        type: cc.Prefab,
        displayName: "菜单按钮预制体"
    })
    btn_prefab: cc.Prefab = null;

    onLoad () {
        this.node.zIndex = 999;
        this.initNavMenu();
    }

    private initNavMenu():void{
        let btnArg = [
            {text:"回城", cb: null},
            {text:"角色", cb: null},
            {text:"背包", cb: this.onBagBtnClick.bind(this)},
            {text:"打造", cb: this.onMakeBtnClick.bind(this)},
            {text:"地图", cb: this.onMapBtnClick.bind(this)},
            {text:"设置", cb: null},
        ];

        btnArg.forEach((val, key) => {
            let node = cc.instantiate(this.btn_prefab);
            let btnView = node.getComponent(MenuBtnView);
            btnView.init(val.text, val.cb);
            this.node.addChild(node);
        })
    }

    public onBagBtnClick():void{
        GameView.instance().showBagWin();
    }

    public onMapBtnClick():void{
        GameView.instance().showMapList();
    }

    public onMakeBtnClick():void{
        GameView.instance().showMakeWin();
    }
}
