import gTipManager, {TipManager} from "../TipManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class TipView extends cc.Component {

    @property(cc.Label)
    text: cc.Label = null;

    action: cc.ActionInterval = null;
    tip_mg: TipManager = null;

    onLoad(){
        this.tip_mg = gTipManager.instance();

        let move = cc.spawn(cc.moveBy(1, 0, 60), cc.fadeOut(1.6));
        this.action = cc.sequence(move, cc.callFunc(()=>{
            this.tip_mg.put(this.node);
            //this.node.removeFromParent(true);
        }));
    }

    start(){
    }

    public show(text: string, color: cc.Color, wPos: cc.Vec2):void{
        this.text.string = text;
        this.node.color = color;
        //let pos = this.node.parent.convertToNodeSpaceAR(wPos);
        this.node.setPosition(wPos);

        this.node.runAction(this.action);
    }

}
