import GameView from "./GameView";
import GameMap from "../Map/GameMap";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MapItemView extends cc.Component {

    m_map: GameMap = null;

    @property({
        type: cc.Node,
        displayName: "地图图片节点"
    })
    map_node: cc.Node = null;

    @property({
        type: cc.Label,
        displayName: "地图名称"
    })
    name_label: cc.Label = null;

    @property({
        type: cc.Label,
        displayName: "等级范围"
    })
    level_label: cc.Label = null;

    public init(map: GameMap):void{
        if(!map){
            return;
        }
        this.m_map = map;
        
        //动态加载物品图标
        let resName = `./map/map_${this.m_map.m_mapId}`;
        cc.resources.load(resName, cc.SpriteFrame, (err, asset) => {
            if (!err) {
                this.map_node.getComponent(cc.Sprite).spriteFrame = <cc.SpriteFrame>asset;
                this.map_node.active = true;
            }
        });

        this.name_label.string = this.m_map.m_mapName;

        this.level_label.string = `${this.m_map.m_minLevel} ~ ${this.m_map.m_maxLevel}`;
    }

    public onClicked():void{
        GameView.instance().showFightWin(this.m_map.m_mapId);
    }
}
