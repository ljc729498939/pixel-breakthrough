import Equip from "../Equip/Equip";
import EquipStarView from "./EquipStarView";
import AttrGroupView from "./AttrGroupView";
import {EquipColor, ConfigType, EquipStatus} from "../Common/GameDefine";
import gConfigManager from "../ConfigManager";
import EquipAttrConfig from "../Config/EquipAttrConfig";
import EquipAttr from "../Equip/EquipAttr";
import ItemConfig from "../Config/ItemConfig";
import AttrColorConfig from "../Config/AttrColorConfig";
import OperatorView from "./OperatorView";
import GameView from "./GameView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class EquipDetailView extends cc.Component {

    m_equip: Equip = null;

    @property({
        type: cc.Node,
        displayName: "内容节点"
    })
    content_node: cc.Node = null;

    @property({
        type: cc.Sprite,
        displayName: "装备图标精灵"
    })
    icon_sprite: cc.Sprite = null;

    @property({
        type: EquipStarView,
        displayName: "装备星级视图"
    })
    star_view: EquipStarView = null;

    @property({
        type: cc.Label,
        displayName: "装备名称标签"
    })
    name_label: cc.Label = null;

    @property({
        type: cc.Node,
        displayName: "装备名称节点"
    })
    name_node: cc.Node = null;

    @property({
        type: cc.Label,
        displayName: "装备描述标签"
    })
    desc_label: cc.Label = null;

    @property({
        type: cc.Prefab,
        displayName: "属性组预制体"
    })
    attr_group_prefab: cc.Prefab = null;

    @property({
        type: cc.Prefab,
        displayName: "操作视图预制体"
    })
    oper_prefab: cc.Prefab = null;

    oper_view: OperatorView = null;

    //属性组
    m_attrGroups: Map<EquipColor, AttrGroupView> = new Map<EquipColor, AttrGroupView>();
    //最高级别颜色的属性组
    m_maxLevelColor: number = 0;

    onLoad(){
        let attrColorCfg: AttrColorConfig = <AttrColorConfig>gConfigManager.getConfigByType(ConfigType.CT_ATTR_COLOR);
        let allCfg = attrColorCfg.getAllCfg();

        allCfg.forEach((val, key) => {
            let node = cc.instantiate(this.attr_group_prefab);
            this.content_node.addChild(node);
            let attrGourp: AttrGroupView = node.getComponent(AttrGroupView);
            attrGourp.init(val);
            this.m_attrGroups.set(val.id, attrGourp);
        });

        let operNode = cc.instantiate(this.oper_prefab);
        this.content_node.addChild(operNode);
        this.oper_view = operNode.children[0].getComponent(OperatorView);
        this.oper_view.init([]);
        this.node.active = false;
    }

    start () {

    }

    public onClicked():boolean{
        //this.reset();
        return false;
    }

    public init(equip: Equip):void{
        if(equip === this.m_equip){
            return;
        }

        this.reset();
        this.m_equip = equip;
        this.node.active = true;

        let itemCfg:ItemConfig = <ItemConfig>gConfigManager.getConfigByType(ConfigType.CT_ITEM);
        let cfg = itemCfg.getCfgById(this.m_equip.m_id);
        //设置名称
        this.name_label.string = cfg.name;
        //设置描述
        this.desc_label.string = cfg.desc;
        //设置星级
        this.star_view.setStarLevel(equip.m_forging);
        //设置装备图标
        this.icon_sprite.spriteFrame = equip.m_itemView.item_sprite.spriteFrame;

        //设置属性
        this.m_equip.m_attrs.forEach((val, key) => {
            this.addAttr(val);
        })

        this.updateNameColor();

        this.initOperBtn();
    }

    private initOperBtn():void{
        if(this.m_equip){
            let args = [];
            if(this.m_equip.m_status === EquipStatus.ES_BAG){
                args.push({name:"装备", callback: ()=>{
                    this.m_equip.use(null);
                    GameView.instance().hideEquipDetail();
                }});
            }
            else if(this.m_equip.m_status === EquipStatus.ES_WEAR){
                args.push({name:"卸下", callback: ()=>{
                    this.m_equip.tackoff();
                    GameView.instance().hideEquipDetail();
                }});
            }

            args.push({name:"打造", callback:()=>{
                GameView.instance().hideEquipDetail();

                //TODO 跳转到打造界面
                GameView.instance().showMakeWin();

            }});

            this.oper_view.init(args);
        }
    }

    public reset():void{
        //清理所有属性
        this.m_attrGroups.forEach((val, key) => {
            val.reset();
        })

        this.m_equip = null;
        this.m_maxLevelColor = 0;

        this.node.active = false;
    }

    private addAttr(attr: EquipAttr):void{
        let attrGroupView: AttrGroupView = this.m_attrGroups.get(attr.m_color);
        if(attrGroupView){
            attrGroupView.addAttr(attr);
            if(attr.m_color > this.m_maxLevelColor){
                this.m_maxLevelColor = attr.m_color;
            }
        }
    }

    private updateNameColor():void{
        let attrGroupView: AttrGroupView = this.m_attrGroups.get(this.m_maxLevelColor);
        if(attrGroupView){
            let color = new cc.Color().fromHEX(attrGroupView.rgb_color);
            this.name_node.color = color;
            console.log(this.name_node);
        }
    }
}
