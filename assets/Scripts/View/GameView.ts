
const {ccclass, property} = cc._decorator;

import gGameManager, { GameManager } from "../GameManager";
import BagView from "./BagView";
import MapListView from "./MapListView";
import EquipDetailView from "./EquipDetailView";
import Equip from "../Equip/Equip";
import MakeView from "./MakeView";
import gAStar from "../AStar/AStar";
import FightView from "./FightView";
import gTipManager from "../TipManager";

@ccclass
export default class GameView extends cc.Component {

    @property(cc.Node)
    main_node: cc.Node = null;

    @property({
        type: cc.Prefab,
        displayName: "背包窗口预制体"
    })
    bag_prefab: cc.Prefab = null;

    @property({
        type: cc.Prefab,
        displayName: "地图列表预制体"
    })
    map_list_prefab: cc.Prefab = null;

    @property({
        type: cc.Prefab,
        displayName: "打造窗口预制体"
    })
    make_prefab: cc.Prefab = null;

    @property({
        type: cc.Prefab,
        displayName: "装备详情预制体"
    })
    equip_detail_prefab: cc.Prefab = null;

    @property({
        type: cc.Prefab,
        displayName: "战斗节点预制体"
    })
    fight_prefab: cc.Prefab = null;

    game_mg: GameManager = null;
    bag_view: BagView = null;
    map_view: MapListView = null;
    make_view: MakeView = null;
    equip_detail_view: EquipDetailView = null;
    fight_view: FightView = null;

    onLoad(){
        //开启物理引擎
        cc.director.getPhysicsManager().enabled = true;
        //开启调试信息
        // cc.director.getPhysicsManager().debugDrawFlags = 
        //     cc.PhysicsManager.DrawBits.e_aabbBit |
        //     cc.PhysicsManager.DrawBits.e_jointBit |
        //     cc.PhysicsManager.DrawBits.e_shapeBit;
        //设置中立加速度为0
        cc.director.getPhysicsManager().gravity = cc.v2();

        cc.director.getCollisionManager().enabled = true;
        //cc.director.getCollisionManager().enabledDebugDraw = true;
        //cc.director.getCollisionManager().enabledDrawBoundingBox = true;

        GameView._instace = this;

        this.game_mg = gGameManager;
        this.game_mg.init();
        gTipManager.instance().init();
        this.initBagView();
        this.initMapListView();
        this.initMakeView();
        this.initFightView();

        this.game_mg.test();
    }

    start () {


    }

    public hideAll():void{
        this.bag_view.node.active = false;
        this.map_view.node.active = false;
        this.make_view.node.active = false;
        this.fight_view.node.active = false;
    }

    public initBagView():void{
        let bagNode = cc.instantiate(this.bag_prefab);
        this.main_node.addChild(bagNode);
        this.bag_view = bagNode.getComponent(BagView);
    }

    public initMakeView():void{
        let node = cc.instantiate(this.make_prefab);
        this.main_node.addChild(node);
        this.make_view = node.getComponent(MakeView);
    }

    public initMapListView():void{
        let mapNode = cc.instantiate(this.map_list_prefab);
        this.main_node.addChild(mapNode);
        this.map_view = mapNode.getComponent(MapListView);
    }

    public initFightView():void{
        let node = cc.instantiate(this.fight_prefab);
        this.main_node.addChild(node);
        this.fight_view = node.getComponent(FightView);
    }

    public showBagWin():void{
        if(!this.bag_view){
            this.initBagView();
        }
        this.hideAll();
        this.bag_view.node.active = true;
    }

    public hideBagWin():void{
        if(this.bag_view){
            this.bag_view.node.active = false;
        }
    }

    public showMakeWin():void{
        if(!this.make_view){
            this.initMakeView();
        }
        this.hideAll();
        this.make_view.node.active = true;
    }

    public showFightWin(mapId: number = 0):void{
        if(!this.fight_view){
            this.initFightView();
        }
        this.hideAll();
        this.fight_view.node.active = true;

        let map = this.game_mg.m_mapManager.getMapById(mapId);
        if(map){
            this.fight_view.init(map);
        }
    }

    public hideMakeWin():void{
        if(this.make_view){
            this.make_view.node.active = false;
        }
    }

    public showMapList():void{
        if(!this.map_view){
            this.initMapListView();
        }
        this.hideAll();
        this.map_view.node.active = true;
    }

    public hideMapList():void{
        if(this.map_view){
            this.map_view.node.active = false;
        }
    }

    public showEquipDetail(equip: Equip):void{
        if(!this.equip_detail_view){
            let node = cc.instantiate(this.equip_detail_prefab);
            this.main_node.addChild(node);
            node.opacity = 220;
            this.equip_detail_view = node.getComponent(EquipDetailView);
        }

        this.equip_detail_view.init(equip);
    }

    public hideEquipDetail():void{
        if(this.equip_detail_view){
            this.equip_detail_view.reset();
        }
    }

    public onInceItem():void{
        let player = gGameManager.m_player;

        let item = player.getItemByIndex(1);
        if(item){
            player.reduceInventory(item, 1);
        }
    }

    private static _instace: GameView = null;
    public static instance(): GameView{
        if(!this._instace){
            this._instace = new GameView();
        }
        return this._instace;
    }
}
