import EquipAttr from "../Equip/EquipAttr";
import gConfigManager from "../ConfigManager";
import { ConfigType } from "../Common/GameDefine";
import EquipAttrConfig from "../Config/EquipAttrConfig";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AttrGroupView extends cc.Component {

    @property({
        type: cc.Label,
        displayName: "组名称标签"
    })
    name_label: cc.Label = null;

    @property({
        type: cc.Node,
        displayName: "内容节点"
    })
    content_node: cc.Node = null;

    @property({
        type: cc.Prefab,
        displayName: "属性项预制体"
    })
    attr_item_prefab: cc.Prefab = null;

    //rgb颜色
    rgb_color: string = "";
    //属性池
    attr_node_pool: cc.NodePool = null;

    onLoad() {
        this.attr_node_pool = new cc.NodePool();
        this.node.active = false;
    }

    start () {

    }

    public init(obj):void{
        this.rgb_color = obj.rgb;
        this.name_label.string = obj.name;
        this.name_label.node.color = new cc.Color().fromHEX(this.rgb_color);
    }

    public reset():void{
        // this.content_node.children.forEach((val, key) => {
        //     this.attr_node_pool.put(val);
        // })

        for(let i = this.content_node.childrenCount - 1; i >= 0 ; --i){
            this.attr_node_pool.put(this.content_node.children[i]);
        }

        this.node.active = false;
    }

    public addAttr(attr:EquipAttr):void{
        if(this.attr_node_pool.size() < 1){
            let tmpNode = cc.instantiate(this.attr_item_prefab);
            tmpNode.color  = new cc.Color().fromHEX(this.rgb_color);
            this.attr_node_pool.put(tmpNode);
        }

        let node = this.attr_node_pool.get();
        this.content_node.addChild(node);

        let label = node.getComponent(cc.Label);
        let equipAttrCfg: EquipAttrConfig = <EquipAttrConfig>gConfigManager.getConfigByType(ConfigType.CT_EQUIP_ATTR);
        let cfg = equipAttrCfg.getCfgById(attr.m_type);

        label.string = `${cfg.name} +${attr.m_value}`;

        this.node.active = true;
    }
}
