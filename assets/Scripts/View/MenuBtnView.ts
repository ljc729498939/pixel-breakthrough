// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuBtnView extends cc.Component {

    @property({
        type: cc.Label,
        displayName: "按钮标签"
    })
    btn_label: cc.Label = null;

    click_cb: ()=>void = null;

    public init(text: string, callback: ()=>void = null): void {
        this.btn_label.string = text;
        this.click_cb = callback;
    }

    public onClicked():void{
        if(this.click_cb){
            this.click_cb();
        }
    }
}
