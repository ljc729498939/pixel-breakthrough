
const {ccclass, property} = cc._decorator;

@ccclass
export default class AttackRangeView extends cc.Component {

    coll_nodes: Array<cc.Node> = [];

    onCollisionEnter(other: cc.Collider, self: cc.Collider){
        this.coll_nodes.push(other.node);
    }

    onCollisionExit(other: cc.Collider, self: cc.Collider){
        let idx = this.coll_nodes.indexOf(other.node);
        if(idx >= 0){
            this.coll_nodes.splice(idx, 1);
        }
    }

    // update (dt) {}
}
