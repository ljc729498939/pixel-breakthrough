import PlayerView from "./PlayerView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FightTouchView extends cc.Component {

    @property(cc.Camera)
    move_camera: cc.Camera = null;

    @property(PlayerView)
    player: PlayerView = null;

    @property(cc.Node)
    root_node: cc.Node = null;

    onLoad () {
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    onTouchEnd(event: cc.Event.EventTouch){
        let v_pos = event.getLocation();

        let w_pos = cc.v2(0, 0);
        //转换成世界坐标
        this.move_camera.getScreenToWorldPoint(v_pos, w_pos);

        //将世界坐标转换成相对player父节点坐标
        let pos = this.root_node.convertToNodeSpaceAR(w_pos);

        //限制用户移动在地图内
        let size = this.root_node.getContentSize();
        let maxH = size.height / 2 - 30;
        let maxW = size.width / 2 - 30;
        pos.x = Math.abs(pos.x) > maxW ? (pos.x > 0 ? maxW : -maxW) : pos.x;
        pos.y = Math.abs(pos.y) > maxH ? (pos.y > 0 ? maxH : -maxH) : pos.y;

        this.player.moveToTarget(pos);
    }
}
