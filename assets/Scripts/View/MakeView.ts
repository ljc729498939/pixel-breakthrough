
const {ccclass, property} = cc._decorator;

@ccclass
export default class MakeView extends cc.Component {

    start () {
        this.node.active = false;
    }

    //锻造点击函数
    public onForgingClicked():void{

    }

    //附魔点击
    public onEnchantingClicked():void{

    }

    //转生点击
    public onReincarnationClicked():void{

    }

    //觉醒点击
    public onAwakenClicked():void{

    }

}
