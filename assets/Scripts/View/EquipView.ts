import Equip from "../Equip/Equip";
import { EquipType } from "../Common/GameDefine";
import GameView from "./GameView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class EquipView extends cc.Component {

    m_equip: Equip = null;

    @property({
        type: cc.Node,
        displayName: "图标节点"
    })
    equip_node: cc.Node = null;

    @property({
        type: cc.Enum(EquipType),
        displayName: "装备类型"
    })
    equip_type: EquipType = EquipType.ET_NONE;

    public onLoad(): void {
        this.equip_node.active = false;
    }

    start() {

    }

    public init(equip: Equip): void {
        this.m_equip = equip;

        //动态加载物品图标
        let resName = `./item/item_${this.m_equip.m_id}`;
        cc.resources.load(resName, cc.SpriteFrame, (err, asset) => {
            if (!err) {
                this.equip_node.getComponent(cc.Sprite).spriteFrame = <cc.SpriteFrame>asset;
                this.equip_node.active = true;
            }
        });
    }

    public clear(): void {
        this.equip_node.active = false;
        this.m_equip = null;
    }

    public onClicked(): void {
        if(this.m_equip){
            //this.m_equip.m_owner.takeOffEquip(this.m_equip.m_equipType);
            GameView.instance().showEquipDetail(this.m_equip);
        }
    }

}
