import Item from "../Item/Item";
import { ItemType } from "../Common/GameDefine";
import GameView from "./GameView";
import Equip from "../Equip/Equip";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemView extends cc.Component {

    m_item: Item = null;

    @property({
        type: cc.Label,
        displayName: "数量标签"
    })
    count_label: cc.Label = null;

    @property({
        type: cc.Sprite,
        displayName: "物品精灵"
    })
    item_sprite: cc.Sprite = null;

    start() {
    }

    init(item: Item):void{
        this.m_item = item;

        //动态加载物品图标
        let resName = `./item/item_${this.m_item.m_id}`;
        cc.resources.load(resName, cc.SpriteFrame, (err, asset) => {
            if (!err) {
                this.item_sprite.spriteFrame = <cc.SpriteFrame>asset;
            }
        });
        this.onChange();
    }

    //发生变化
    public onChange(remove:boolean = false):void{
        if(this.m_item){
            if(remove){
                this.node.removeFromParent();
            }
            else{
                if(this.m_item.m_isStacking){
                    this.count_label.string = "x" + this.m_item.m_count;
                }
                else{
                    this.count_label.string = "";
                }
            }
        }
    }

    public onClicked(): void {
        if(this.m_item.m_type === ItemType.IT_EQUIP){
            GameView.instance().showEquipDetail(<Equip>this.m_item);
        }
        else{
            this.m_item.use(null);
        }
    }

    // update (dt) {}
}
