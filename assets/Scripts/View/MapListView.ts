import MapItemView from "./MapItemView";
import gConfigManager from "../ConfigManager";
import { ConfigType } from "../Common/GameDefine";
import gGameManager from "../GameManager";
import MapManager from "../MapManager";
import GameMap from "../Map/GameMap";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MapListView extends cc.Component {

    @property({
        type: cc.Node,
        displayName: "内容节点"
    })
    content_node: cc.Node = null;

    @property({
        type: cc.Prefab,
        displayName: "地图节点预制体"
    })
    map_item_prefab: cc.Prefab = null;

    public start(): void {
        this.initMapList();
        this.node.active = false;
    }

    public initMapList():void{
        let mapManager: MapManager = gGameManager.m_mapManager;
        if(mapManager){
            mapManager.m_maps.forEach((val:GameMap, key:number) => {
                let node = cc.instantiate(this.map_item_prefab);
                this.content_node.addChild(node);
                let mapItemView = node.getComponent(MapItemView);
                mapItemView.init(val);
            });
        }
    }
}
