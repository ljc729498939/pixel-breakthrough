import GameView from "./GameView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MaskView extends cc.Component {

    onLoad () {
        this.node.on(cc.Node.EventType.TOUCH_END, this.onClicked, this);
    }

    start () {

    }

    public onClicked(e){
        GameView.instance().hideEquipDetail();
    }

    // update (dt) {}
}
