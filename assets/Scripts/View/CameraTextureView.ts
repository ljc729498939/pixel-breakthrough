
const {ccclass, property} = cc._decorator;

@ccclass
export default class CameraTextureView extends cc.Component {

    @property(cc.Camera)
    camera: cc.Camera = null;

    start () {
        let sp:cc.Sprite = this.getComponent(cc.Sprite);
        let texture: cc.RenderTexture = new cc.RenderTexture(); 
        texture.initWithSize(this.node.width, this.node.height);
        let spriteFrame: cc.SpriteFrame = new cc.SpriteFrame();
        spriteFrame.setTexture(texture);
        sp.spriteFrame = spriteFrame;

        this.camera.targetTexture = texture;
    }
}
