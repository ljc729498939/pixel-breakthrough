import PlayerView from "./PlayerView";
import MapView from "./MapView";
import gConfigManager from "../ConfigManager";
import { ConfigType } from "../Common/GameDefine";
import CameraView from "./CameraView";
import gGameManager from "../GameManager";
import {GameManager} from "../GameManager";
import MapConfig from "../Config/MapConfig";
import MapManager from "../MapManager";
import GameMap from "../Map/GameMap";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FightView extends cc.Component {

    @property(CameraView)
    camera_view: CameraView = null;

    @property(PlayerView)
    player: PlayerView = null;

    @property(MapView)
    map: MapView = null;

    @property(cc.Prefab)
    player_prefab: cc.Prefab = null;

    onLoad () {
        
    }

    start () {
        this.node.active = false;
    }

    public init(map: GameMap):void{

        if(map){
            this.initMap(map);
            this.initPlayer();
        }
    }

    //初始化地图
    private initMap(map: GameMap):void{
        this.map.init(map);
    }

    private initPlayer():void{
        //let gameMg:GameManager = gGameManager.instance();
        this.player.init(gGameManager.m_player);
        //this.player.node.setPosition(cc.v2(0, 0));
    }
}
