<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="tiled_1" tilewidth="32" tileheight="32" tilecount="1024" columns="32">
 <image source="tiled_1.png" width="1024" height="1024"/>
 <terraintypes>
  <terrain name="绿草地" tile="0"/>
  <terrain name="墨绿草地" tile="0"/>
  <terrain name="水" tile="0"/>
  <terrain name="草地中土地" tile="0"/>
  <terrain name="石子路1" tile="0"/>
  <terrain name="石子路2" tile="0"/>
  <terrain name="石子路3" tile="0"/>
  <terrain name="石子路4" tile="0"/>
  <terrain name="石子路5" tile="0"/>
  <terrain name="地洞1" tile="0"/>
  <terrain name="地洞2" tile="0"/>
  <terrain name="地洞3" tile="0"/>
  <terrain name="挤出-绿" tile="0"/>
  <terrain name="挤出-碎" tile="0"/>
  <terrain name="挤出-黄" tile="0"/>
  <terrain name="芦苇头" tile="0"/>
  <terrain name="沙地" tile="0"/>
  <terrain name="岩浆" tile="0"/>
  <terrain name="土路1" tile="0"/>
  <terrain name="土路2" tile="0"/>
  <terrain name="砖块地-内" tile="0"/>
  <terrain name="砖块地-外" tile="0"/>
  <terrain name="地下室" tile="0"/>
  <terrain name="新建地形" tile="0"/>
 </terraintypes>
 <tile id="5" terrain=",,,23"/>
 <tile id="6" terrain=",,23,23"/>
 <tile id="7" terrain=",,23,"/>
 <tile id="8" terrain=",,,23"/>
 <tile id="9" terrain=",,23,"/>
 <tile id="16" terrain="4,4,4,"/>
 <tile id="17" terrain="4,4,,4"/>
 <tile id="19" terrain="5,5,5,"/>
 <tile id="20" terrain="5,5,,5"/>
 <tile id="22" terrain="0,0,0,"/>
 <tile id="23" terrain="0,0,,0"/>
 <tile id="25" terrain="9,9,9,"/>
 <tile id="26" terrain="9,9,,9"/>
 <tile id="28" terrain="11,11,11,"/>
 <tile id="29" terrain="11,11,,11"/>
 <tile id="37" terrain=",23,,23"/>
 <tile id="38" terrain="23,23,23,23"/>
 <tile id="39" terrain="23,,23,"/>
 <tile id="40" terrain=",23,,"/>
 <tile id="41" terrain="23,,,"/>
 <tile id="48" terrain="4,,4,4"/>
 <tile id="49" terrain=",4,4,4"/>
 <tile id="51" terrain="5,,5,5"/>
 <tile id="52" terrain=",5,5,5"/>
 <tile id="54" terrain="0,,0,0"/>
 <tile id="55" terrain=",0,0,0"/>
 <tile id="57" terrain="9,,9,9"/>
 <tile id="58" terrain=",9,9,9"/>
 <tile id="60" terrain="11,,11,11"/>
 <tile id="61" terrain=",11,11,11"/>
 <tile id="69" terrain=",23,,"/>
 <tile id="70" terrain="23,23,,"/>
 <tile id="71" terrain="23,,,"/>
 <tile id="79" terrain=",,,4"/>
 <tile id="80" terrain=",,4,4"/>
 <tile id="81" terrain=",,4,"/>
 <tile id="82" terrain=",,,5"/>
 <tile id="83" terrain=",,5,5"/>
 <tile id="84" terrain=",,5,"/>
 <tile id="85" terrain=",,,0"/>
 <tile id="86" terrain=",,0,0"/>
 <tile id="87" terrain=",,0,"/>
 <tile id="88" terrain=",,,9"/>
 <tile id="89" terrain=",,9,9"/>
 <tile id="90" terrain=",,9,"/>
 <tile id="91" terrain=",,,11"/>
 <tile id="92" terrain=",,11,11"/>
 <tile id="93" terrain=",,11,"/>
 <tile id="111" terrain=",4,,4"/>
 <tile id="112" terrain="4,4,4,4"/>
 <tile id="113" terrain="4,,4,"/>
 <tile id="114" terrain=",5,,5"/>
 <tile id="115" terrain="5,5,5,5"/>
 <tile id="116" terrain="5,,5,"/>
 <tile id="117" terrain=",0,,0"/>
 <tile id="118" terrain="0,0,0,0"/>
 <tile id="119" terrain="0,,0,"/>
 <tile id="120" terrain=",9,,9"/>
 <tile id="121" terrain="9,9,9,9"/>
 <tile id="122" terrain="9,,9,"/>
 <tile id="123" terrain=",11,,11"/>
 <tile id="124" terrain="11,11,11,11"/>
 <tile id="125" terrain="11,,11,"/>
 <tile id="143" terrain=",4,,"/>
 <tile id="144" terrain="4,4,,"/>
 <tile id="145" terrain="4,,,"/>
 <tile id="146" terrain=",5,,"/>
 <tile id="147" terrain="5,5,,"/>
 <tile id="148" terrain="5,,,"/>
 <tile id="149" terrain=",0,,"/>
 <tile id="150" terrain="0,0,,"/>
 <tile id="151" terrain="0,,,"/>
 <tile id="152" terrain=",9,,"/>
 <tile id="153" terrain="9,9,,"/>
 <tile id="154" terrain="9,,,"/>
 <tile id="155" terrain=",11,,"/>
 <tile id="156" terrain="11,11,,"/>
 <tile id="157" terrain="11,,,"/>
 <tile id="181" terrain="0,0,0,0"/>
 <tile id="182" terrain="0,0,0,0"/>
 <tile id="183" terrain="0,0,0,0"/>
 <tile id="205" terrain="10,10,10,"/>
 <tile id="206" terrain="10,10,,10"/>
 <tile id="208" terrain="17,17,17,"/>
 <tile id="209" terrain="17,17,,17"/>
 <tile id="211" terrain="8,8,8,"/>
 <tile id="212" terrain="8,8,,8"/>
 <tile id="213" terrain="0,0,0,"/>
 <tile id="214" terrain="0,0,,0"/>
 <tile id="215" terrain="0,0,,0"/>
 <tile id="237" terrain="10,,10,10"/>
 <tile id="238" terrain=",10,10,10"/>
 <tile id="240" terrain="17,,17,17"/>
 <tile id="241" terrain=",17,17,17"/>
 <tile id="243" terrain="8,,8,8"/>
 <tile id="244" terrain=",8,8,8"/>
 <tile id="245" terrain="0,,0,0"/>
 <tile id="246" terrain=",0,0,0"/>
 <tile id="247" terrain=",0,0,0"/>
 <tile id="268" terrain=",,,10"/>
 <tile id="269" terrain=",,10,10"/>
 <tile id="270" terrain=",,10,"/>
 <tile id="271" terrain=",,,17"/>
 <tile id="272" terrain=",,17,17"/>
 <tile id="273" terrain=",,17,"/>
 <tile id="274" terrain=",,,8"/>
 <tile id="275" terrain=",,8,8"/>
 <tile id="276" terrain=",,8,"/>
 <tile id="277" terrain=",,,0"/>
 <tile id="278" terrain=",,0,0"/>
 <tile id="279" terrain=",,0,"/>
 <tile id="289" terrain="16,16,16,"/>
 <tile id="290" terrain="16,16,,16"/>
 <tile id="292" terrain="16,16,16,2"/>
 <tile id="293" terrain="16,16,2,16"/>
 <tile id="295" terrain="2,2,2,0"/>
 <tile id="296" terrain="2,2,0,2"/>
 <tile id="298" terrain="2,2,2,"/>
 <tile id="299" terrain="2,2,,2"/>
 <tile id="300" terrain=",10,,10"/>
 <tile id="301" terrain="10,10,10,10"/>
 <tile id="302" terrain="10,,10,"/>
 <tile id="303" terrain=",17,,17"/>
 <tile id="304" terrain="17,17,17,17"/>
 <tile id="305" terrain="17,,17,"/>
 <tile id="306" terrain=",8,,8"/>
 <tile id="307" terrain="8,8,8,8"/>
 <tile id="308" terrain="8,,8,"/>
 <tile id="309" terrain=",0,,0"/>
 <tile id="310" terrain="0,0,0,0"/>
 <tile id="311" terrain="0,,0,"/>
 <tile id="321" terrain="16,,16,16"/>
 <tile id="322" terrain=",16,16,16"/>
 <tile id="324" terrain="16,2,16,16"/>
 <tile id="325" terrain="2,16,16,16"/>
 <tile id="327" terrain="2,0,2,2"/>
 <tile id="328" terrain="0,2,2,2"/>
 <tile id="330" terrain="2,,2,2"/>
 <tile id="331" terrain=",2,2,2"/>
 <tile id="332" terrain=",10,,"/>
 <tile id="333" terrain="10,10,,"/>
 <tile id="334" terrain="10,,,"/>
 <tile id="335" terrain=",17,,"/>
 <tile id="336" terrain="17,17,,"/>
 <tile id="337" terrain="17,,,"/>
 <tile id="338" terrain=",8,,"/>
 <tile id="339" terrain="8,8,,"/>
 <tile id="340" terrain="8,,,"/>
 <tile id="341" terrain=",0,,"/>
 <tile id="342" terrain="0,0,,"/>
 <tile id="343" terrain="0,,,"/>
 <tile id="352" terrain=",,,16"/>
 <tile id="353" terrain=",,16,16"/>
 <tile id="354" terrain=",,16,"/>
 <tile id="355" terrain="2,2,2,16"/>
 <tile id="356" terrain="2,2,16,16"/>
 <tile id="357" terrain="2,2,16,2"/>
 <tile id="358" terrain="0,0,0,2"/>
 <tile id="359" terrain="0,0,2,2"/>
 <tile id="360" terrain="0,0,2,0"/>
 <tile id="361" terrain=",,,2"/>
 <tile id="362" terrain=",,2,2"/>
 <tile id="363" terrain=",,2,"/>
 <tile id="384" terrain=",16,,16"/>
 <tile id="385" terrain="16,16,16,16"/>
 <tile id="386" terrain="16,,16,"/>
 <tile id="387" terrain="2,16,2,16"/>
 <tile id="388" terrain="16,16,16,16"/>
 <tile id="389" terrain="16,2,16,2"/>
 <tile id="390" terrain="0,2,0,2"/>
 <tile id="391" terrain="2,2,2,2"/>
 <tile id="392" terrain="2,0,2,0"/>
 <tile id="393" terrain=",2,,2"/>
 <tile id="394" terrain="2,2,2,2"/>
 <tile id="395" terrain="2,,2,"/>
 <tile id="416" terrain=",16,,"/>
 <tile id="417" terrain="16,16,,"/>
 <tile id="418" terrain="16,,,"/>
 <tile id="419" terrain="2,16,2,2"/>
 <tile id="420" terrain="16,16,2,2"/>
 <tile id="421" terrain="16,2,2,2"/>
 <tile id="422" terrain="0,2,0,0"/>
 <tile id="423" terrain="2,2,0,0"/>
 <tile id="424" terrain="2,0,0,0"/>
 <tile id="425" terrain=",2,,"/>
 <tile id="426" terrain="2,2,,"/>
 <tile id="427" terrain="2,,,"/>
 <tile id="486" terrain="3,3,3,0"/>
 <tile id="487" terrain="3,3,0,3"/>
 <tile id="518" terrain="3,0,3,3"/>
 <tile id="519" terrain="0,3,3,3"/>
 <tile id="521" terrain=",,,15"/>
 <tile id="522" terrain=",,15,15"/>
 <tile id="523" terrain=",,15,"/>
 <tile id="549" terrain="0,0,0,3"/>
 <tile id="550" terrain="0,0,3,3"/>
 <tile id="551" terrain="0,0,3,0"/>
 <tile id="553" terrain=",15,,15"/>
 <tile id="554" terrain="15,15,15,15"/>
 <tile id="555" terrain="15,,15,"/>
 <tile id="581" terrain="0,3,0,3"/>
 <tile id="582" terrain="3,3,3,3"/>
 <tile id="583" terrain="3,0,3,0"/>
 <tile id="585" terrain=",15,,"/>
 <tile id="586" terrain="15,15,,"/>
 <tile id="587" terrain="15,,,"/>
 <tile id="613" terrain="0,3,0,0"/>
 <tile id="614" terrain="3,3,0,0"/>
 <tile id="615" terrain="3,0,0,0"/>
 <tile id="641" terrain="12,12,12,"/>
 <tile id="642" terrain="12,12,,12"/>
 <tile id="644" terrain="13,13,13,"/>
 <tile id="645" terrain="13,13,,13"/>
 <tile id="648" terrain="18,18,18,18"/>
 <tile id="649" terrain="18,18,18,18"/>
 <tile id="650" terrain="18,18,18,18"/>
 <tile id="651" terrain="18,18,18,18"/>
 <tile id="652" terrain="18,18,18,18"/>
 <tile id="673" terrain="12,,12,12"/>
 <tile id="674" terrain=",12,12,12"/>
 <tile id="676" terrain="13,,13,13"/>
 <tile id="677" terrain=",13,13,13"/>
 <tile id="678" terrain="15,15,15,"/>
 <tile id="679" terrain="15,15,,15"/>
 <tile id="680" terrain="18,18,18,19"/>
 <tile id="681" terrain="18,18,19,19"/>
 <tile id="682" terrain="18,18,19,18"/>
 <tile id="683" terrain="19,19,19,18"/>
 <tile id="684" terrain="19,19,18,19"/>
 <tile id="704" terrain=",,,12"/>
 <tile id="705" terrain=",,12,12"/>
 <tile id="706" terrain=",,12,"/>
 <tile id="707" terrain=",,,13"/>
 <tile id="708" terrain=",,13,13"/>
 <tile id="709" terrain=",,13,"/>
 <tile id="710" terrain="15,,15,15"/>
 <tile id="711" terrain=",15,15,15"/>
 <tile id="712" terrain="18,19,18,19"/>
 <tile id="713" terrain="19,19,19,19"/>
 <tile id="714" terrain="19,18,19,18"/>
 <tile id="715" terrain="19,18,19,19"/>
 <tile id="716" terrain="18,19,19,19"/>
 <tile id="725" terrain="20,20,20,20"/>
 <tile id="726" terrain="20,20,20,20"/>
 <tile id="727" terrain="20,20,20,20"/>
 <tile id="728" terrain="20,20,20,20"/>
 <tile id="729" terrain="20,20,20,20"/>
 <tile id="736" terrain=",12,,12"/>
 <tile id="737" terrain="12,12,12,12"/>
 <tile id="738" terrain="12,,12,"/>
 <tile id="739" terrain=",13,,13"/>
 <tile id="740" terrain="13,13,13,13"/>
 <tile id="741" terrain="13,,13,"/>
 <tile id="744" terrain="18,19,18,18"/>
 <tile id="745" terrain="19,19,18,18"/>
 <tile id="746" terrain="19,18,18,18"/>
 <tile id="755" terrain="20,20,20,21"/>
 <tile id="756" terrain="20,20,21,21"/>
 <tile id="757" terrain="20,20,21,20"/>
 <tile id="758" terrain="21,21,21,20"/>
 <tile id="759" terrain="21,21,20,21"/>
 <tile id="768" terrain=",12,,"/>
 <tile id="769" terrain="12,12,,"/>
 <tile id="770" terrain="12,,,"/>
 <tile id="771" terrain=",13,,"/>
 <tile id="772" terrain="13,13,,"/>
 <tile id="773" terrain="13,,,"/>
 <tile id="775" terrain="1,1,1,"/>
 <tile id="776" terrain="1,1,,1"/>
 <tile id="787" terrain="20,21,20,21"/>
 <tile id="788" terrain="21,21,21,21"/>
 <tile id="789" terrain="21,20,21,20"/>
 <tile id="790" terrain="21,20,21,21"/>
 <tile id="791" terrain="20,21,21,21"/>
 <tile id="807" terrain="1,,1,1"/>
 <tile id="808" terrain=",1,1,1"/>
 <tile id="819" terrain="20,21,20,20"/>
 <tile id="820" terrain="21,21,20,20"/>
 <tile id="821" terrain="21,20,20,20"/>
 <tile id="832" terrain=",,14,14"/>
 <tile id="833" terrain="14,14,14,"/>
 <tile id="834" terrain="14,14,,14"/>
 <tile id="836" terrain="6,6,6,"/>
 <tile id="837" terrain="6,6,,6"/>
 <tile id="838" terrain=",,,1"/>
 <tile id="839" terrain=",,1,1"/>
 <tile id="840" terrain=",,1,"/>
 <tile id="864" terrain="14,14,,"/>
 <tile id="865" terrain="14,,14,14"/>
 <tile id="866" terrain=",14,14,14"/>
 <tile id="868" terrain="6,,6,6"/>
 <tile id="869" terrain=",6,6,6"/>
 <tile id="870" terrain=",1,,1"/>
 <tile id="871" terrain="1,1,1,1"/>
 <tile id="872" terrain="1,,1,"/>
 <tile id="896" terrain=",,,14">
  <objectgroup draworder="index">
   <object id="1" x="1" y="31.75">
    <polygon points="0,0 0,-5.5 2.75,-11.25 7,-14.5 11.5,-15.25 15.5,-17 30.75,-17.25 31,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="897" terrain=",,14,14">
  <objectgroup draworder="index">
   <object id="1" x="-0.25" y="15.75">
    <polygon points="0,0 32,0.25 32.25,16.5 0,16.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="898" terrain=",,14,">
  <objectgroup draworder="index">
   <object id="1" x="0.25" y="16.5">
    <polygon points="0,0 20.75,-1 25.75,2.75 29.5,7.5 31.25,12 31.25,14.75 31.25,15 31.25,15.5 -0.25,15.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="899" terrain=",,,6"/>
 <tile id="900" terrain=",,6,6"/>
 <tile id="901" terrain=",,6,"/>
 <tile id="902" terrain=",1,,"/>
 <tile id="903" terrain="1,1,,"/>
 <tile id="904" terrain="1,,,"/>
 <tile id="928" terrain=",14,,14">
  <objectgroup draworder="index">
   <object id="1" x="0.75" y="0.5">
    <polygon points="0,0 0.25,31.25 31,31.5 31.25,-0.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="929" terrain="14,14,14,14">
  <objectgroup draworder="index">
   <object id="1" x="-0.25" y="0.5">
    <polygon points="0,0 0.25,31.25 32,31.5 31.75,-0.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="930" terrain="14,,14,"/>
 <tile id="931" terrain=",6,,6"/>
 <tile id="932" terrain="6,6,6,6"/>
 <tile id="933" terrain="6,,6,"/>
 <tile id="934" terrain="1,1,1,1"/>
 <tile id="935" terrain="1,1,1,1"/>
 <tile id="936" terrain="1,1,1,1"/>
 <tile id="944" terrain=",,,22"/>
 <tile id="945" terrain=",,22,22"/>
 <tile id="946" terrain=",,22,"/>
 <tile id="947" terrain="22,22,22,"/>
 <tile id="948" terrain="22,22,,22"/>
 <tile id="960" terrain=",14,,">
  <objectgroup draworder="index">
   <object id="1" x="0.5" y="1">
    <polygon points="0,0 0,19.5 3.75,25.75 10.5,28.75 18.25,30.25 31,30 31,-1"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="961" terrain="14,14,,">
  <objectgroup draworder="index">
   <object id="1" x="-0.75" y="0.5">
    <polygon points="0,0 0.5,30.25 32.25,30.25 32.5,-0.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="962" terrain="14,,,">
  <objectgroup draworder="index">
   <object id="1" x="-0.25" y="30.25">
    <polygon points="0,0 24.5,-0.5 32,-9 32.25,-30.25 0,-30.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="963" terrain=",6,,"/>
 <tile id="964" terrain="6,6,,"/>
 <tile id="965" terrain="6,,,"/>
 <tile id="976" terrain=",22,,22"/>
 <tile id="977" terrain="22,22,22,22"/>
 <tile id="978" terrain="22,,22,"/>
 <tile id="979" terrain="22,,22,22"/>
 <tile id="980" terrain=",22,22,22"/>
 <tile id="1008" terrain=",22,,"/>
 <tile id="1009" terrain="22,22,,"/>
 <tile id="1010" terrain="22,,,"/>
</tileset>
